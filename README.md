<center><img src="logos/logo-nom.png" width=200 height=253/></center>

A simple [chaos monkey](https://en.wikipedia.org/wiki/Chaos_engineering) implementation for Kubernetes.

This implementation was created as a complete exercice of my Kubernetes Training ([Program in french](https://www.retengr.com/formations-devops/kubernetes/)).

A Killer randomly kills Pods in your cluster. You configure your killer using labels to indicate wich pods can be selected as a victim.

You can declare as many killers as you want, each of them with its own victim configuration.

You can easily give a shield to a Pod to avoid being killed using "kubekiller: protected" label.


## Video

Understand KubeKiller with a video : https://youtu.be/ZlWiu2rT9ts


## Install


The `KubeKiller` is deployed using [HELM](https://helm.sh)


- Declare the chart registry :
  ```
  helm repo add rtg-chart http://chart.infra.retengr.com
  ```

- Install the application :
  ```
  helm install kubekiller rtg-chart/kubekiller
  ```

- Install demo
  ```
  helm install kubekiller-demo rtg-chart/kubekiller-demo
  ```
> Resources of this demo can be viewed [there](https://gitlab.com/Retengr/kubekiller-demo/-/tree/master/templates)

## Uninstall

```
helm uninstall kubekiller
helm uninstall kubekiller-demo
```


## Declaring a KubeKiller

Once the `kubekiller` application has been installed, we can now declare a `KubeKiller`.

A `KubeKiller` is managed as a traditional Kubernetes Resource, You can delare it using the following syntax :

```killer1
cat << EOF | kubectl apply -f -
apiVersion: stable.rtg.com/v1
kind: KubeKiller
metadata:
  name: killer1
spec:
  delay: 10           # Delay between 2 attacks
  victimsSelectors:       # List of labels that must be set on you victims pods
    -  chaosmonkey: back 
       app: demo1
    -  chaosmonkey: front
       app: demo1
EOF
```
> In the previous example, only Pods with  (`chaosmonkey: back` AND `app: demo1`)  **OR** (`chaosmonkey: front` AND `app: demo1`) might be killed by `killer1`.

## Protect some Pods

If you want to protect some Pods, you may declare "kubekiller: protected" in its labels.

## Contact

[Denis Peyrusaubes](mailto:denis@retengr.com?subject=[KubeKiller]

## TODOLIST

Once deployed, A KubeKiller needs two minutes to initiate its process. That might be improve : WIP.



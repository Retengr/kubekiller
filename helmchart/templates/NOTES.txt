Deploy a `RtgMonkey` resource using

cat << EOF | kubectl apply -f -
apiVersion: stable.rtg.com/v1
kind: KubeKiller
metadata:
  app: thekiller
spec:
  delay: 10           # Delay between 2 attacks
  victimsSelectors:       # Labels to designate your victims
    -  chaosmonkey: back 
       app: demo1
    -  chaosmonkey: front
       app: demo1
EOF

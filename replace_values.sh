#!/bin/sh

export THEKILLERVERSION=0.0.4
export CHARTVERSION=0.0.4
export CHARTAPPVERSION=0.0.4
sed -i.bak 's/__THEKILLERVERSION__/'"$THEKILLERVERSION"'/g' killer-manager/build_image.sh
sed -i.bak 's/__THEKILLERVERSION__/'"$THEKILLERVERSION"'/g' helmchart/values.yaml
sed -i.bak 's/__CHARTVERSION__/'"$CHARTVERSION"'/g' helmchart/Chart.yaml
sed -i.bak 's/__CHARTAPPVERSION__/'"$CHARTAPPVERSION"'/g' helmchart/Chart.yaml
